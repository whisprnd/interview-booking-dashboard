import React, { Component } from 'react';
import './App.scss';

export default class App extends Component {
    render() {
        return (
            <div className='app'>
                <div className="page-content">
                    <h1>What are you waiting for?</h1>
                </div>
            </div>
        );
    }
}

